kamoso (22.12.3-2) unstable; urgency=medium

  * Team upload.
  * Remove inactive Uploaders.
  * Modernize building:
    - add the dh-sequence-kf5 build dependency to use the kf5 addon
      automatically, removing pkg-kde-tools
    - drop all the manually specified addons and buildsystem for dh
  * CI: enable again blhc, as it works fine now with blhc 0.13.
  * Re-export upstream signing key without extra signatures.
  * Mark the xauth, and xvfb build dependencies as !nocheck, as they are needed
    only during dh_auto_test.
  * Add the gstreamer1.0-plugins-bad dependency, needed for at least
    wrappercamerabinsrc. (Closes: #984705)

 -- Pino Toscano <pino@debian.org>  Sun, 12 Mar 2023 07:21:23 +0100

kamoso (22.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.3).

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 01 Mar 2023 11:57:45 +0100

kamoso (22.12.1-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.1).
  * Bump Standards-Version to 4.6.2, no change required.
  * Add Albert Astals Cid’s master key to upstream signing keys.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 06 Jan 2023 22:33:53 +0100

kamoso (22.12.0-2) unstable; urgency=medium

  * Upload with a correctly released changelog.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 16:04:29 +0100

kamoso (22.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.08.3).
  * Bump Standards-Version to 4.6.1, no change required.
  * New upstream release (22.11.90).
  * New upstream release (22.12.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 00:12:50 +0100

kamoso (21.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (21.12.3).
  * Bump Standards-Version to 4.6.0, no change required.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 06 Mar 2022 20:05:27 +0100

kamoso (21.08.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.08.0).

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 15:47:50 +0900

kamoso (21.04.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.04.0).
  * Borrow minimal upstream signing key from k3b.
  * Drop Kubuntu from maintainer name.
  * Added myself to the uploaders.

 -- Norbert Preining <norbert@preining.info>  Tue, 27 Apr 2021 11:19:20 +0900

kamoso (20.12.1-1) unstable; urgency=medium

  * New upstream release (20.12.1).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 07 Jan 2021 22:56:09 +0100

kamoso (20.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * Add upstream metadata.
  * New upstream release (20.12.0).
  * Update build dependencies according to the upstream build system:
    - GStreamer >= 1.10
  * Add missing QML rumtime dependencies.
  * Refresh copyright information.
  * Bump Standards-Version to 4.5.1, no change required.
  * Refresh upstream metadata.
  * Removed Maximiliano Curia from the uploaders, thanks for your work
    on the package!
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 02 Jan 2021 11:56:20 +0100

kamoso (20.08.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (20.08.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 27 Oct 2020 00:10:56 +0100

kamoso (20.08.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Pino Toscano <pino@debian.org>  Thu, 03 Sep 2020 16:46:57 +0200

kamoso (20.08.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump KF packages to 5.70.0

 -- Pino Toscano <pino@debian.org>  Fri, 14 Aug 2020 12:02:51 +0200

kamoso (20.04.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop the Debian man page, as it is outdated, and it offers no useful
    information.
  * Explicitly add the gettext build dependency.
  * Add the configuration for the CI on salsa.

 -- Pino Toscano <pino@debian.org>  Mon, 03 Aug 2020 11:46:58 +0200

kamoso (20.04.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (20.04.1).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 05 Jun 2020 11:51:35 +0200

kamoso (20.04.0-1) experimental; urgency=medium

  * Team upload.

  [ Aurélien COUDERC ]
  * New upstream release (20.04.0):
    - Bump KDE frameworks required version to 5.63 or greater.
  * Update debian/watch to new release-service URL.
  * Fix typo in CC0-1.0 header stanza in debian/copyright.
  * Borrow minimal upstream signing key from k3b.
  * Refresh and simplify copyright information.
  * Remove explicit --as-needed from linker flags, it’s now injected by
    default.
  * Bump standards version to 4.5.0 (no change required).
  * Bump debhelper compat level to 13.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 04 May 2020 15:53:42 +0200

kamoso (19.08.3-2) unstable; urgency=medium

  * Add missing build dependency to libkf5notifications-dev.
  * Make build dependencies to KDE Frameworks >= 5.56 according to the
    upstream build system.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 15 Dec 2019 00:02:14 +0100

kamoso (19.08.3-1) unstable; urgency=medium

  * Team upload.

  [ Aurélien COUDERC ]
  * New upstream release (19.08.3).
  * Move debian/copyright to machine readable format 1.0 and complete
    review.
  * debian/control:
    - move to debhelper-compat, bump version to 12
    - set Rules-Requires-Root: no
    - point to new Homepage URL
  * Add hardening=+all build option.
  * Bump standards version to 4.4.1 (no change needed).
  * Add upstream signing key and signature check in debian/watch.

  [ Maximiliano Curia ]
  * New revision

  [ John Scott ]
  * Add qml-module-org-kde-kirigami2 dependency (Closes: #914441).

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 14 Dec 2019 09:15:59 +0100

kamoso (18.04.0-3) unstable; urgency=medium

  * Team upload.
  * Make the test suite non-fatal for now, as there are failures (in LLVM?)
    at least on armhf, and mips64el.

 -- Pino Toscano <pino@debian.org>  Sun, 22 Apr 2018 23:21:22 +0200

kamoso (18.04.0-2) unstable; urgency=medium

  * Team upload.
  * Bump the libkf5purpose-dev build dependency to >= 5.44.0~, to make sure
    to build with the Frameworks version of it.

 -- Pino Toscano <pino@debian.org>  Sun, 22 Apr 2018 11:37:59 +0200

kamoso (18.04.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file, as it is part of Applications now.
  * Update the build dependencies:
    - remove libkf5declarative-dev, libqt5gstreamer-dev, and libudev-dev:
      no more needed
    - add libqt5opengl5-dev, and qtdeclarative5-dev
    - bump the version of libkf5purpose-dev to >= 5.44.0~, to make sure the
      Frameworks version is used
  * Switch Maintainer to Debian/Kubuntu Qt/KDE Maintainers, like the other
    sources of Applications.
  * Bump Priority to optional, like generally used (also extra is deprecated).
  * Switch Vcs-* fields to salsa.debian.org.
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
    - remove --parallel for dh, as now done by default
  * Make sure the test suite can be run:
    - add the xauth, and xvfb build dependencies
    - run dh_auto_test under xvfb-run
    - add the gstreamer1.0-plugins-base build dependency, since it contains
      gstreamer elements used by the test suite
  * Bump Standards-Version to 4.1.4, no changes required.
  * Switch Homepage to https://userbase.kde.org/Kamoso.

 -- Pino Toscano <pino@debian.org>  Sun, 22 Apr 2018 08:58:46 +0200

kamoso (3.2.4-1) unstable; urgency=medium

  * Team upload.

  [ Rohan Garg ]
  * New upstream release

  [ Pino Toscano ]
  * Update Vcs-* fields.
  * Replace deprecated build dependencies:
    - kdoctools-dev -> libkf5doctools-dev
    - kio-dev -> libkf5kio-dev
  * Remove extra shared-desktop-ontologies build dependency.
  * Bump Standards-Version to 4.0.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Mon, 19 Jun 2017 21:47:04 +0200

kamoso (3.2.1-1) unstable; urgency=medium

  [ Pino Toscano ]
  * New upstream release.

  [ Rohan Garg ]
  * Fix watch file to take into account the xz extension
  * Add libkf5purpose-dev to build-depends
  * Drop all patches, irrelevant or applied upstream
  * Update rules
    - No need for special case handling anymore
  * Update Build-Depends
  * Drop kipi-plugins from Recommends
  * Add Depends on qml-modules-org-kde-purpose

  [ Maximiliano Curia ]
  * Set myself as uploader
  * Bump Standards-Version to 3.9.8, no changes needed.

 -- Maximiliano Curia <maxy@debian.org>  Tue, 06 Dec 2016 21:03:15 +0100

kamoso (2.0.2-3.1) unstable; urgency=medium

  * Non-maintainer upload.
     Also include /usr/lib/<triplet>/gstreamer-<version>/include.
     (Closes: #801592)

 -- YunQiang Su <syq@debian.org>  Mon, 12 Oct 2015 18:11:57 +0800

kamoso (2.0.2-3) unstable; urgency=medium

  * Team upload.

  [ Diane Trout ]
  * Update watch file to http://downloads.kde.org
  * Backport upstream commit b85e8c187b4eabf9765bc05348f6232ba32c9493
    to build against QtGstreamer 1.0 API
  * Change qt-gstreamer build and installation depends to 1.0 API.
  * Bump Standards-Version to 3.9.6, no changes required.
  * Add fix-qt-gstreamer-1.0.diff to apply a few hacks to
    get the pipeline actually open the webcam and write files.

 -- Diane Trout <diane@ghic.org>  Wed, 27 May 2015 20:07:29 -0700

kamoso (2.0.2-2) unstable; urgency=low

  * Team upload.

  [ José Manuel Santamaría Lema ]
  * Bump debhelper compatibility level to 9. This enables the hardening flags.

  [ Pino Toscano ]
  * Fix the Vcs-* headers.
  * Backport upstream commit d3c5946f9f48f5d7a5249995424228fc51c84b67 to
    include a needed header; patch upstream_add-include.patch.
  * Backport upstream commits a0aec903b36b52bda1a9a5d2e875af1063747a86 and
    9f32257a1e930b16e64508e6fbfa9390fc0322ee to fix compatibility with libkipi
    2.x; patch upstream_libkipi-2.x.diff.
  * Backport part of upstream commit b8b03322d58a920deac198c2360d65deddccd610
    to rename the youtube icon to kipiplugin_youtube; patch
    upstream_rename-icons.diff. Since renaming sources is not viable, rename
    the actual icons at installation time.
  * Bump Standards-Version to 3.9.4, no changes required.
  * Link in as-needed mode.

 -- Pino Toscano <pino@debian.org>  Fri, 19 Jul 2013 15:42:37 +0200

kamoso (2.0.2-1) unstable; urgency=low

  * Initial release (Closes: #577639)

 -- José Manuel Santamaría Lema <panfaust@gmail.com>  Sun, 24 Jul 2011 01:20:38 +0200
